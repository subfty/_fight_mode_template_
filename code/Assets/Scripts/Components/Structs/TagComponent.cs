﻿using Entitas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class TagComponent : IComponent {
    #region Tags
    public const string PLAYER = "player";
    public const string FAT_KID = "fatkid";

    #region Enemies
    public const string HOLOGRAM = "hologram";
    public const string EVIL_FAT_KID = "evilfatkid";
    public const string AGU_BEAUTYEARFLY = "agu_beautyearfly";
    public const string AGU_SUBMARINE = "agu_submarine";
    #endregion

    public const string TAG_LEVEL_MAP = "TAG_LEVEL_MAP";    
    public const string DIALOG_PREFIX = "DIALOG_";
    public const string CHOICE_BOX = "CHOICE_BOX";
    public const string TAG_SCREEN_FADER = "SCREEN_FADER";
    public const string TAG_FIGHT_MODE = "TAG_FIGHT_MODE";
    public const string TAG_TERMINAL_UI = "TAG_TERMINAL_UI";
    public const string TAG_FIGHT_MINI_GAME = "TAG_FIGHT_MINI_GAME";
    public const string TAG_BOOT_SCREEN = "bootstarted";
    public const string TAG_MAIN_MENU = "TAG_MAIN_MENU";
    public const string TAG_MAIN_MENU_BOX = "TAG_MAIN_MENU_BOX";
    public const string TAG_TIME_COUNTER = "TIME_COUNTER";
    public const string TAG_HUD_BACKPACK = "TAG_HUD_BACKPACK";
    #endregion

    static Group _TaggedGroup = null;

    static Group TaggedGroup {
        get {
            if (_TaggedGroup == null)
                _TaggedGroup = Pools.pool.GetGroup(Matcher.Tag);
            return _TaggedGroup;
        }
    }               

    public static Entity GetEntityWithTag(string tag) { 
       //TODO: speed this shit up!        

        foreach (var e in TaggedGroup.GetEntities())
            if (e.tag.Tag == tag)
                return e;
        
        return null;
    }

    public static List<Entity> GetEntitiesWithTagPrefix(string prefix) {        
        List<Entity> result = new List<Entity>();

        foreach (var e in TaggedGroup.GetEntities())
            if (e.tag.Tag.StartsWith(prefix))
                result.Add(e);        

        return result;
    }

    public static int CountEntitiesWithTagPrefix(string prefix) {                
        int count = 0;
        foreach (var e in TaggedGroup.GetEntities()) {
            if (e.tag.Tag.StartsWith(prefix))
                count++;
        }
        return count;
    }

    public string Tag;    
}

