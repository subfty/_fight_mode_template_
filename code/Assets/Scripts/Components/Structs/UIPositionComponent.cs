﻿using Entitas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class UIPositionComponent : IComponent {
    public float X;
    public float Y;
    public float Z;
    public bool WorldPos;
}
