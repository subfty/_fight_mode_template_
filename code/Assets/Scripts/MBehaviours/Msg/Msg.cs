﻿using UnityEngine;
using System.Collections;
using Entitas;
using System.Collections.Generic;
using System;

public class Msg {    

    public class InputEvent : TinyMessenger.ITinyMessage {
        private static InputEvent _Instance = null;

        #region Implementation
        public bool JustPressed;
        
        public static InputEvent Get(bool justPressed) {
            if (_Instance == null)
                _Instance = new InputEvent();
            _Instance.JustPressed = justPressed;
            return _Instance;
        }        

        public object Sender {
            get { return null; }
        }
        #endregion
    }  
    
    public class MoveCameraAnimated : TinyMessenger.ITinyMessage {
        #region Implementation
        private static MoveCameraAnimated _Instance = null;

        public float Duration;                
        
        public static MoveCameraAnimated Get(float duration) {
            if (_Instance == null)
                _Instance = new MoveCameraAnimated();
            _Instance.Duration = duration;
            return _Instance;
        }
        
        public object Sender {
            get { return null; }
        }
        #endregion
    }                   

    public class FinishedLoadingLevel : TinyMessenger.ITinyMessage {
        #region Implementation        
        private static FinishedLoadingLevel _Instance = null;

        public FinishedLoadingLevel() {}
        public static FinishedLoadingLevel Get() {
            if (_Instance == null)
                _Instance = new FinishedLoadingLevel();

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class SetMenuBoxButtonIsLocked : TinyMessenger.ITinyMessage {
        #region Implementation
        private static SetMenuBoxButtonIsLocked _Instance = null;
        public bool IsLocked;

        public static SetMenuBoxButtonIsLocked Get(bool isLocked) {
            if(_Instance == null)
                _Instance = new SetMenuBoxButtonIsLocked();
            _Instance.IsLocked = isLocked;

            return _Instance;
        }
        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class LaunchMenuBoxAnimation : TinyMessenger.ITinyMessage {
        #region Implementation
        private static LaunchMenuBoxAnimation _Instance = null;
        public bool Entrance;

        public static LaunchMenuBoxAnimation Get(bool entrance) {
            if (_Instance == null)
                _Instance = new LaunchMenuBoxAnimation();
            _Instance.Entrance = entrance;

            return _Instance;
        }        

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class StateChanged : TinyMessenger.ITinyMessage {
        #region Implementation
        private static StateChanged _Instance = null;
        public string KeyValue;
        public string NewValue;        

        public static StateChanged Get(string keyValue, string newValue) {
            if (_Instance == null)
                _Instance = new StateChanged();

            _Instance.KeyValue = keyValue;
            _Instance.NewValue = newValue;

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }
        
    public class ItemOwnershipChanged : TinyMessenger.ITinyMessage {
        #region Implementation
        private static ItemOwnershipChanged _Instance = null;

        public static ItemOwnershipChanged Get() {
            if (_Instance == null)
                _Instance = new ItemOwnershipChanged();

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class PauseSystemExecution : TinyMessenger.ITinyMessage {
        #region Implementation
        private static PauseSystemExecution _Instance = null;
        public bool EnablePause;

        public static PauseSystemExecution Get(bool enablePause) {
            if (_Instance == null)
                _Instance = new PauseSystemExecution();

            _Instance.EnablePause = enablePause;
            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class PauseWorld : TinyMessenger.ITinyMessage {
        #region Implementation
        private static PauseWorld _Instance = null;
        public bool EnablePause;

        public static PauseWorld Get(bool enablePause) {
            if (_Instance == null)
                _Instance = new PauseWorld();

            _Instance.EnablePause = enablePause;
            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class NeedToUpdatePositions : TinyMessenger.ITinyMessage {
        #region Implementation
        private static NeedToUpdatePositions _Instance = null;

        public static NeedToUpdatePositions Get() {
            if (_Instance == null)
                _Instance = new NeedToUpdatePositions();
            
            return _Instance;
        }

        public object Sender { 
            get { return null; } 
        }
        #endregion
    }  
       
    public class FreezeMovableCharacters : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FreezeMovableCharacters _Instance = null;
        public int Might;
        
        public static FreezeMovableCharacters Get(int might = 1) {
            if (_Instance == null)
                _Instance = new FreezeMovableCharacters();

            _Instance.Might = might;

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }        
    
    public class SmoothOutIncomingCameraTransition : TinyMessenger.ITinyMessage {
        #region Implementation
        public float TransitionTime;
        private static SmoothOutIncomingCameraTransition _Instance = null;

        public static SmoothOutIncomingCameraTransition Get(float transitionTime = 1.0f) {
            if (_Instance == null)
                _Instance = new SmoothOutIncomingCameraTransition();
            _Instance.TransitionTime = transitionTime;

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }
    
    public class FinishCameraTransitionAnimation : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FinishCameraTransitionAnimation _Instance = null;

        public static FinishCameraTransitionAnimation Get() {
            if (_Instance == null)
                _Instance = new FinishCameraTransitionAnimation();

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }            

    public class FreezeDialogExecution : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FreezeDialogExecution _Instance = null;
        public bool DoFreeze;
        public bool DoHideDialogWindow;        

        public static FreezeDialogExecution Get(bool doFreeze, bool doHide = true) {
            if (_Instance == null)
                _Instance = new FreezeDialogExecution();

            _Instance.DoFreeze = doFreeze;
            _Instance.DoHideDialogWindow = doHide;

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class NotifyEntity : TinyMessenger.ITinyMessage {
        #region Implementation
        private static NotifyEntity _Instance = null;
        public string EntityTag;
        public string[] Parameters;

        public static NotifyEntity Get(string entityTag, params string[] parameters) {
            if (_Instance == null)
                _Instance = new NotifyEntity();

            _Instance.EntityTag = entityTag;
            _Instance.Parameters = parameters;

            return _Instance;
        }        

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class DialogFinished : TinyMessenger.ITinyMessage {
        #region Implementation
        private static DialogFinished _Instance = null;

        public static DialogFinished Get() {
            if (_Instance == null)
                _Instance = new DialogFinished();
            return _Instance;
        }        
        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class FightMiniGameFinished : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FightMiniGameFinished _Instance = null;

        public float LastedTimePercent;

        public static FightMiniGameFinished Get(float lastedTimePercent) {
            if (_Instance == null)
                _Instance = new FightMiniGameFinished();

            _Instance.LastedTimePercent = lastedTimePercent;

            return _Instance;
        }
                
        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class FightGameProgress : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FightGameProgress _Instance = null;

        public float Progress;

        public static FightGameProgress Get(float progress) {
            if (_Instance == null)
                _Instance = new FightGameProgress();

            progress = Mathf.Clamp01(progress);
            _Instance.Progress = progress;

            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class FollowingPathFinished : TinyMessenger.ITinyMessage {
        #region Implementation
        private static FollowingPathFinished _Instance = null;
        public Entity ForEntity;

        public static FollowingPathFinished Get(Entity forEntity) {
            if (_Instance == null)
                _Instance = new FollowingPathFinished();

            _Instance.ForEntity = forEntity;
            return _Instance;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class EntityMovedToTile : TinyMessenger.ITinyMessage {
        #region Implementation
        private static EntityMovedToTile _Instance = null;
        public Entity ForEntity;
        public Vector3 NewPosition;
        
        public static EntityMovedToTile Get(Entity forEntity, Vector3 newPosition) {
            if(_Instance == null)
                _Instance = new EntityMovedToTile();

            _Instance.ForEntity = forEntity;
            _Instance.NewPosition = new Vector3(
                newPosition.x, 
                newPosition.y, 
                newPosition.z);

            return _Instance;
        }        

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class EntityBumpedOnTile : TinyMessenger.ITinyMessage {
        #region Implementation
        private static EntityBumpedOnTile _Instance = null;
        public Entity ForEntity;
        public Vector3 BumpedTile;
        public bool WasMoving;

        // returns self for easier chaining
        public static EntityBumpedOnTile Get(Entity forEntity, Vector3 position, bool wasMoving) {
            if (_Instance == null)
                _Instance = new EntityBumpedOnTile();

            _Instance.ForEntity = forEntity;
            _Instance.BumpedTile = new Vector3(
                position.x,
                position.y,
                position.z);
            _Instance.WasMoving = wasMoving;

            return _Instance;
        }        

        public object Sender {
            get { return null; }
        }
        #endregion
    }

    public class NotifyTriggers : TinyMessenger.ITinyMessage {
        #region Implementation
        private static NotifyTriggers _Instance = null;
        public string Message;

        // returns self for easier chaining
        public static NotifyTriggers Get(string message) {
            if (_Instance == null)
                _Instance = new NotifyTriggers();

            _Instance.Message = message;            

            return _Instance;
        }

        public static NotifyTriggers Clone(NotifyTriggers msg) {
            NotifyTriggers clone = new NotifyTriggers();
            clone.Message = msg.Message;
            return clone;
        }

        public object Sender {
            get { return null; }
        }
        #endregion
    }
}
