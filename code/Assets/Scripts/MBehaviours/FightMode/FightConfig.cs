﻿using Entitas;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;

[RequireComponent(typeof(EMonoBehaviour))]
public class FightConfig : MonoBehaviour {
    public const int SPECIAL_GEM_START_AT = 999;    

    public enum GemType {
        EMPTY = 0,
        REGULAR_0 = 1,
        REGULAR_1,
        REGULAR_2,
        REGULAR_3,
        REGULAR_4,

        SPECIAL_DUMMY = SPECIAL_GEM_START_AT,
    }
    
}

