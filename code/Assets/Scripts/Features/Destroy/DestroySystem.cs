﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class DestroySystem : IReactiveSystem, ISetPool {

    public TriggerOnEvent trigger { get { return Matcher.Destroy.OnEntityAdded(); } }

    Pool _pool;

    public void SetPool(Pool pool) {
        _pool = pool;
        pool.GetGroup(Matcher.GameObject).OnEntityRemoved += onEntityRemoved;        
    }

    void onEntityRemoved(Group group, Entity entity, int index, IComponent component) {
        var viewComponent = (GameObjectComponent)component;
        Object.Destroy(viewComponent.obj);
    }

    public void Execute(List<Entity> entities) {
        foreach (var e in entities) {
            if(e.hasGameObject)
                e.RemoveGameObject();
            if(_pool.HasEntity(e))
                _pool.DestroyEntity(e);
        }
    }
}