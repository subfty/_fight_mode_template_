﻿using Entitas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;

class InputSystem : IExecuteSystem, ISetPool {
    Group _Group;
    bool _LastPressedState = false;

    bool _JustTouched = false;
    bool _LastTouched = false;

    Vector3 _LastMousePosition = Vector3.zero;

    public void SetPool(Pool pool) {
        _Group = pool.GetGroup(Matcher.AllOf(Matcher.InputReceiver));
    }

    public void Execute() {
        if (GameController.Instance.PauseGame) return;

        bool pressed =
            Input.touchCount > 0 ||
            Input.GetMouseButton(0) || 
            Input.GetKey(KeyCode.Space);
        if (Input.touchCount > 0){
            if (!_LastTouched) {
                if (_JustTouched) {
                    _JustTouched = false;
                    _LastTouched = true;
                } else {
                    _JustTouched = true;
                }
            }
        } else {
            _JustTouched = false;
            _LastTouched = false;
        }         

        foreach (var e in _Group.GetEntities()) {    
            bool changed = false;
            bool justPressed = e.inputReceiver.JustPressed;
            bool justReleased = e.inputReceiver.JustReleased;            
            //float holdFillSpeed = e.inputReceiver.HoldFillSpeed;
            //float releaseFillSpeed = e.inputReceiver.ReleaseFillSpeed;
            //TODO: implement properly
            float holdProgress = pressed ? 1.0f : 0.0f;       

            if (pressed != _LastPressedState) {
                if (pressed) {
                    justPressed = true;
                    justReleased = false;
                } else {
                    justPressed = false;
                    justReleased = true;
                }
            } else 
                justPressed = justReleased = false;            

            if (justPressed != e.inputReceiver.JustPressed ||
                justReleased != e.inputReceiver.JustReleased)
                changed = true;

            if (changed || pressed) {
                e.ReplaceInputReceiver(
                    e.inputReceiver.HoldFillSpeed,
                    e.inputReceiver.ReleaseFillSpeed,
                    holdProgress,
                    justPressed,
                    justReleased
                    );                
            }
        }        
    
        if(!_LastPressedState && pressed)
            TinyMessengerHub
                .Instance
                .Publish<Msg.InputEvent>(Msg.InputEvent.Get(true));
        if (_LastPressedState && !pressed)
            TinyMessengerHub
                .Instance
                .Publish<Msg.InputEvent>(Msg.InputEvent.Get(false));

        _LastPressedState = pressed;

        if (GameController.Instance.PauseGame) {
            _JustTouched = false;
            _LastTouched = false;
        }
    }    
}