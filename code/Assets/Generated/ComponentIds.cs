public static class ComponentIds {
    public const int Destroy = 0;
    public const int DontDestroyOnLevelLoad = 1;
    public const int GameObject = 2;
    public const int InputReceiver = 3;
    public const int NestedView = 4;
    public const int Resource = 5;
    public const int Tag = 6;
    public const int UIPosition = 7;

    public const int TotalComponents = 8;

    public static readonly string[] componentNames = {
        "Destroy",
        "DontDestroyOnLevelLoad",
        "GameObject",
        "InputReceiver",
        "NestedView",
        "Resource",
        "Tag",
        "UIPosition"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(DestroyComponent),
        typeof(DontDestroyOnLevelLoad),
        typeof(GameObjectComponent),
        typeof(InputReceiverComponent),
        typeof(NestedViewComponent),
        typeof(ResourceComponent),
        typeof(TagComponent),
        typeof(UIPositionComponent)
    };
}