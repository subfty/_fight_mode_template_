using System.Collections.Generic;

namespace Entitas {
    public partial class Entity {
        public InputReceiverComponent inputReceiver { get { return (InputReceiverComponent)GetComponent(ComponentIds.InputReceiver); } }

        public bool hasInputReceiver { get { return HasComponent(ComponentIds.InputReceiver); } }

        static readonly Stack<InputReceiverComponent> _inputReceiverComponentPool = new Stack<InputReceiverComponent>();

        public static void ClearInputReceiverComponentPool() {
            _inputReceiverComponentPool.Clear();
        }

        public Entity AddInputReceiver(float newHoldFillSpeed, float newReleaseFillSpeed, float newTouchHold, bool newJustPressed, bool newJustReleased) {
            var component = _inputReceiverComponentPool.Count > 0 ? _inputReceiverComponentPool.Pop() : new InputReceiverComponent();
            component.HoldFillSpeed = newHoldFillSpeed;
            component.ReleaseFillSpeed = newReleaseFillSpeed;
            component.TouchHold = newTouchHold;
            component.JustPressed = newJustPressed;
            component.JustReleased = newJustReleased;
            return AddComponent(ComponentIds.InputReceiver, component);
        }

        public Entity ReplaceInputReceiver(float newHoldFillSpeed, float newReleaseFillSpeed, float newTouchHold, bool newJustPressed, bool newJustReleased) {
            var previousComponent = hasInputReceiver ? inputReceiver : null;
            var component = _inputReceiverComponentPool.Count > 0 ? _inputReceiverComponentPool.Pop() : new InputReceiverComponent();
            component.HoldFillSpeed = newHoldFillSpeed;
            component.ReleaseFillSpeed = newReleaseFillSpeed;
            component.TouchHold = newTouchHold;
            component.JustPressed = newJustPressed;
            component.JustReleased = newJustReleased;
            ReplaceComponent(ComponentIds.InputReceiver, component);
            if (previousComponent != null) {
                _inputReceiverComponentPool.Push(previousComponent);
            }
            return this;
        }

        public Entity RemoveInputReceiver() {
            var component = inputReceiver;
            RemoveComponent(ComponentIds.InputReceiver);
            _inputReceiverComponentPool.Push(component);
            return this;
        }
    }

    public partial class Matcher {
        static IMatcher _matcherInputReceiver;

        public static IMatcher InputReceiver {
            get {
                if (_matcherInputReceiver == null) {
                    var matcher = (Matcher)Matcher.AllOf(ComponentIds.InputReceiver);
                    matcher.componentNames = ComponentIds.componentNames;
                    _matcherInputReceiver = matcher;
                }

                return _matcherInputReceiver;
            }
        }
    }
}
